\select@language {magyar}
\contentsline {chapter}{Előszó}{1}{chapter*.2}
\contentsline {part}{\numberline {I}A MECHANIKA ALAPJAI}{3}{part.1}
\contentsline {chapter}{Prológus}{5}{chapter*.3}
\contentsline {chapter}{\numberline {1}A newtoni mechanika}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Tömeg és erő}{8}{section.1.1}
\contentsline {section}{\numberline {1.2}Az első törvény}{10}{section.1.2}
\contentsline {section}{\numberline {1.3}A második törvény}{12}{section.1.3}
\contentsline {section}{\numberline {1.4}A tömeg és a súly különböznek...}{19}{section.1.4}
\contentsline {section}{\numberline {1.5}...de azért annyira mégsem}{20}{section.1.5}
\contentsline {section}{\numberline {1.6}A harmadik törvény}{22}{section.1.6}
\contentsline {section}{\numberline {1.7}A szimmetria csapdái}{26}{section.1.7}
\contentsline {section}{\numberline {1.8}Az erők eredője}{29}{section.1.8}
\contentsline {addendum}{\numberline {1$\mathclose {\setbox \z@ \hbox {\frozen@everymath \@emptytoks \mathsurround \z@ $\nulldelimiterspace \z@ \left /\vcenter to\@ne \big@size {}\right .$}\box \z@ }$A.}Nem nagy tudomány}{33}{addendum.1}
\contentsline {addendum}{\numberline {1$\mathclose {\setbox \z@ \hbox {\frozen@everymath \@emptytoks \mathsurround \z@ $\nulldelimiterspace \z@ \left /\vcenter to\@ne \big@size {}\right .$}\box \z@ }$B.}Súly kontra tömeg}{38}{addendum.2}
\contentsline {problems}{\numberline {1}}{43}{problems.1}
\contentsline {chapter}{\numberline {2}A centrális erők Newton-féle elemzése}{59}{chapter.2}
\contentsline {problems}{\numberline {2}}{71}{problems.2}
\contentsline {chapter}{\numberline {3}Megmaradási törvények}{85}{chapter.3}
\contentsline {section}{\numberline {3.1}Az impulzusmegmaradás}{86}{section.3.1}
\contentsline {section}{\numberline {3.2}Az impulzusmomentum-megmaradás}{88}{section.3.2}
\contentsline {section}{\numberline {3.3}Az energiamegmaradás: a kinetikus-, és a potenciális energia}{93}{section.3.3}
\contentsline {section}{\numberline {3.4}Energiamegmaradás ütközések során}{100}{section.3.4}
\contentsline {chapter}{\numberline {4}Az egytest-, és a kéttest-probléma}{101}{chapter.4}
\contentsline {chapter}{\numberline {5}Merev testek}{103}{chapter.5}
\contentsline {chapter}{\numberline {6}Kényszerek}{105}{chapter.6}
\contentsline {chapter}{\numberline {7}Filozófiai és történeti kérdések}{107}{chapter.7}
\contentsline {part}{\numberline {II}ÉPÍTKEZÉS AZ ALAPOKRA}{109}{part.2}
\contentsline {chapter}{\numberline {8}Oszcillációk}{111}{chapter.8}
\contentsline {chapter}{\numberline {9}A merev testek mozgása}{113}{chapter.9}
\contentsline {chapter}{\numberline {10}Nem-inerciarendszerek és tehetetlenségi erők}{115}{chapter.10}
\contentsline {chapter}{\numberline {11}Súrlódás -- barát és ellenség}{117}{chapter.11}
\contentsline {part}{\numberline {III}LAGRANGE-FÉLE MECHANIKA}{119}{part.3}
\contentsline {chapter}{\numberline {12}Analitikus mechanika}{121}{chapter.12}
\contentsline {chapter}{\numberline {13}Variációs elvek}{123}{chapter.13}
\contentsline {chapter}{\numberline {14}Kis oszcillációk}{125}{chapter.14}
\contentsline {part}{KÖZJÁTÉK}{127}{chapter.14}
\contentsline {chapter}{\numberline {15}A fény}{129}{chapter.15}
\contentsline {part}{\numberline {IV}HAMILTON-FÉLE MECHANIKA}{131}{part.4}
\contentsline {chapter}{Az aragonittól a Schrödinger-egyenletig}{133}{chapter*.56}
\contentsline {chapter}{\numberline {16}A koérintőnyaláb}{135}{chapter.16}
\contentsline {chapter}{\numberline {17}A mechanika és az optika összjátéka}{137}{chapter.17}
\contentsline {chapter}{\numberline {18}A Hamilton--Jacobi-elmélet}{139}{chapter.18}
\contentsline {chapter}{\numberline {19}Kanonikus transzformációk}{141}{chapter.19}
\contentsline {chapter}{\numberline {20}Szimplektikus sokaságok}{143}{chapter.20}
\contentsline {chapter}{\numberline {21}A Liouville-féle integrálhatóság}{145}{chapter.21}
\contentsline {chapter}{\numberline {22}Epilógus}{147}{chapter.22}
\contentsline {chapter}{Parciális differenciálegyenletekről röviden}{149}{chapter*.57}
\contentsfinish 
