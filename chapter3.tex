\chapter{Megmaradási törvények}

\lettrine[lines=2]{E}{}bben a fejezetben a mechanika alapvető ,,megmaradási'' törvényeit
fogjuk tekinteni, amelyek közül az impulzusmegmaradást már futólag említettük az 1. fejezetben.
Az ottani meggondolások arra sarkallhatnak, hogy ,,részecskerenszerekkel'' foglalkozzunk,
amelyek következőkből állnak:
\begin{enumerate}[(a)]\indented
\item bizonyos részecskék $\Map{c_1,\ldots,c_K}{\Real}{\Real^3}$,
\item amelyek pozitív tömegei $m_1,\ldots,m_K\in\Real$,
\item $\Map{\fie{F}^e_i}{\Real}{\Real^3}$ függvények,
\item $\Map{\fie{F}_{ij}=-\fie{F}_{ji}}{\Real}{\Real^3}$ függvények,
\end{enumerate}

\noindent amelyek a következő alaptulajdonsággal rendelkeznek: Ha
\[
\fie{F}_i=\fie{F}_i^e+\sum_j \fie{F}_{ij},
\]
akkor
\[
\fie{F}_i=m_i\cdot c_i''.
\]
Itt $\fie{F}_i^e(t)$ a $t$ időpillanatban a $c_i$ részecskére ható ,,külső erőt''
jelöli, míg $\fie{F}_{ij}(t)$ a $c_i(t)$ és $c_j(t)$ közt ható ,,belső erőket''
modellezi, amelyek kielégítik Newton harmadik törvényét. Következésképpen
$\fie{F}_i(t)$ a $t$ időpillanatban a $c_i$ részecskére ható összerő.
Az olyan erők, amelyek kielégítik Newton harmadik törvényének ,,erős alakját''
(\pageref{harmadikerosalak}. oldal), akkor a (d) feltételben még azt is ki kell kötnünk,
hogy $\fie{F}_{ij}(t)$ a $c_i(t)-c_j(t)$ számszorosa.

Persze a gyakorlatban az $\fie{F}_i^e$ és $\fie{F}_{ij}$ mennyiségek egyszerű alakban
írhatók egyéb függvények segítségével. Például, az $\fie{F}^e_i$ külső erő lehet
$m_i\cdot \fie{f}(c_i(t))$ alakú adott $\Real^3$ fölötti $\fie{f}$ vektormező esetén,
mondjuk egy másik, külső test általi gravitációs vonzás, míg az $\fie{F}_{ij}$ belső erők
lehetnek $m_i$, $m_j$ és a $|c_i-c_j|$ távolság valamilyen függvényei.

Az áltanos definíciónk megenged mindenféle komplikált szituációt, például azt, amikor
az $\fie{F}_i^e$ erő nem csak $t$-től és $c_i(t)$-től függ, hanem az összes
$\{c_j(t)\}$-tól. Erre egy egyszerű példa egy űrhajó lehet, amelyet sok részecske
reprezentál, és az erő iránya függ attól, hogy az adott pillanatban éppen milyen
irányba fordul az ürhajó. (Az űrhajónak feltehetően több, mint egy rakétája van, azért,
hogy tudjon kanyarodni.)

\section{Az impulzusmegmaradás} 
Ugyan az 1. fejezetben az impulzusmegmaradás csupán belső erőkkel foglalkozott
könnyen általánosíthatjuk külső erőkre is. Legyen $\fie{F}=\sum_i\fie{F}_i^e$
a külső erők összege.

\begin{prop}[IMPULZUSTÉTEL]
Az összimpulzus deriváltja a külső erők összege,
\[
\fie{F}=\Bigg(\sum_i m_i\cdot \fie{v}_i\Bigg)'.
\]
Itt úgy kell tekintenünk az egyes $\fie{F}_i^e$ vektorokra, mint $\Real^3$ elemeire,
és nem $\Real^3$ különböző pontjaiban felvett érintővektorokra, és ugyanez érvényes
a $\fie{v}_i$-kre is.
\end{prop}

Az iménti megfogalmazás jelentősége nyilvánvaló lesz, ha bevezetjük a $\{c_i\}$ rendszer
\textbf{tömegközéppontjának} fogalmát, ami $c_i$ részecskék tömegük szerint súlyozott
,,átlagos'' helyét írja le:
\[
C=\frac{\sum_im_i\cdot c_i}{\sum_i m_i}.
\]
Precízebben fogalmazva, a tömegközéppontot egy részecskének definiáljuk, amely 
helyzete $C$ és tömege $M=\sum_i m_i$.

Ha nincsenek külső erők, azaz $\fie{F}_i^e=0$, vagyis $\sum_im_i\fie{v}_i$ konstans, akkor
$C''=\frac{1}{M}\sum_im_i\cdot c_i''=\frac{1}{M}\sum_im_i\fie{v}_i'=
\frac{1}{M}\Big(\sum_i m_i\cdot \fie{v}_i\Big)'$ és így $C''=0$. Ennélfogva $C'$
állandó; szavakkal, a tömegközéppont egyenletes sebességgel halad.

Általában a következő érvényes.

\begin{prop}
Ha $\fie{F}=\sum_i\fie{F}_i^e$ a külső erők összege, akkor
\[
\fie{F}=M\cdot C\,'',
\]
így a tömegközéppont, mint részecske, egyszerűen úgy mozog, mintha egy $\fie{F}$
összerő hatna rá.
\end{prop}
\begin{proofnoqed}
Közvetlen számolással:
\begin{align*}
M\cdot C\,''&=\sum_im_i\cdot c_i''\\
&=\sum_i \fie{F}_i\\
&=\sum_i \fie{F}_i^e + \sum_i\sum_j \fie{F}_{ij}\\
&=\sum_i \fie{F}_i^e\qed
\end{align*}
\end{proofnoqed}

Bár a $C$ ,,részecske'' nem felétlenül egy valódi részecske a rendszerünkben,
az iménti eredményt mégsem tekintik kimondottan ,,elméletinek'' --
lehetővé teszi ugyanis, hogy egy nagyon bonyolult rendszerről egy roppant egyszerű
képet kapjunk. Például, \apageref{lejtohajit}. oldalon lévő ábrán egy
bonyolult forgómozgást végző rudat láthatunk, ám a tömegközéppont, amely
éppenséggel egy pont a rúdon, egyszerűen egy parabolpályán mozog, akár egy tömegpont.
Egy látványos illusztrációt kapunk, ha egy levegőbe feldobott pálcáról
hosszú expozícióidejű fényképet készítünk, és a pálca végeire, illetve tömegközéppontjára
egy-egy lámpát helyezünk. Valami ilyesmi képet kapunk:
\begin{center}
\includegraphics[scale=0.2]{fig/botparabola.png}
\end{center}

Általában a rudat ,,merev testnek'' tekintjük, ezzel a fogalommal még mindig nem 
mertünk foglalkozni. Első ránézésre ez a fogalom még lenyűgözőbbé teszi az impulzustörvény
állítását: egy valódi rúdban mindenféle intermolekuláris erők lépnek fel, amelyek
hozzávetőleg ,,merevvé'' teszik, de nem teljesen, és még így is igaz, hogy
a tömegközéppont egy egyszerű törvény szerint mozog. 
Ám ez némiképp félrevezető értelmezése az eredménynek, elvégre éppen a rúd merevsége
tette lehetővé, hogy megtaláljuk a tömegközéppontját a rúdon, és odaragasszuk az egyik lámpát.

A tömegközéppontot szokás ,,súlypontnak'' is nevezni, ez a fogalom pedig legalább
Archimedes-ig nyúlik vissza (vö. a Prológussal). A két fogalom általában nem ugyanaz,
csak konstans gravitációs térben -- ami tehát ésszerű léptékű tárgyakra érvényes a Föld
felszínén -- ám ezt a különbséget gyakran figyelmen kívül hagyják.

\section{Az impulzusmomentum-megmaradás}
Az előző fejezet legelején használtuk a $\times$ vektoriális szorzatot, bár ott 
végsősoron tekinthető a determinánsokkal végzett műveletek egy kényelmes rövidítésének.
Ennek az $\Real^3$-on értelmezett speciális szorzatnak a jelentősége valójában a következő.

Bármely rögzített $\fie{w}\in\Real^3$ vektor esetén tekintsük a $\Map{B(t)}{\Rs}{\Rs}$
egyparaméteres transzformációcsaládot, ahol $B(t)$ az óramutató járásával ellentétes 
irányba történő $t|\fie{w}|$ radiánnal való forgatás a $\fie{w}$ tengely körül
[ahol úgy választjuk meg a $\fie{w}$-re merőleges sík $(\fie{v}_1,\fie{v}_2)$ irányítását,
hogy a $(\fie{v}_1,\fie{v}_2,\fie{w})$ az $\Rs$ szokásos irányítását adja].
Most tekintsük ezen egyparaméteres család által generált vektormezőt. 
Tehát minden $\fie{p}\in\Rs$ esetén tekintsük a $B_{\fie{p}}(t)=B(t)\fie{p}$ görbét,
és vegyük ennek a görbének az $X_{\fie{p}}$ érintővektorát a 0-ban.
\begin{center}
\includegraphics[scale=0.3]{fig/fogataserinto.png}
\end{center}

Az $X_{\fie{p}}$ geometriai meghatározásához vegyük észre, hogy $X_{\fie{p}}$
nyilván merőleges $\fie{p}$-re és $\fie{w}$-re is. A hosszát is könnyű meghatározni.
Ha $\fie{p}$ éppen a $\fie{w}$-re merőleges síkba esik, mint az (a) ábrán,
akkor a $\fie{p}$ pont egy $|\fie{p}|$ sugarú körön forog körbe és $X_{\fie{p}}$ 
hossza $|\fie{p}|\cdot |\fie{w}|$. Általában (b), a $\fie{p}$ pont egy
$|\fie{p}|\cdot|\fie{w}|\cdot\sin\theta$ sugarú körön forog körbe, ahol 
$\theta$ a $\fie{w}$ és $\fie{p}$ által közrezárt szög. Tehát $X_{\fie{p}}$
egyszerűen a geometriailag deiniált $\fie{w}\times\fie{p}$ vektoriális szorzat.
\begin{center}
\includegraphics[scale=0.3]{fig/forgataserinto.png}
\end{center}

Az $X_{\fie{p}}=B_{\fie{p}}'(0)$ analitikus meghatározásához vegyük észre, 
hogy mivel mindegyik $B_{\fie{p}}(t)$ ortogonális, és $B_{\fie{p}}(0)=I$, ezért
a $B_{\fie{p}}'(0)$ derivált antiszimmetrikus, legyen $M$, és
\[
M=\begin{pmatrix}
0 & -\omega_3 & \omega_2\\
\omega_3 & 0 & -\omega_1\\
-\omega_2 & \omega_1 & 0
\end{pmatrix}.
\]
Ekkor az $X_{\fie{p}}$ vektor egy számhármas, amelynek az $X_{\fie{p}}^\trans$ transzponáltját
a következőképpen lehet megadni:
\begin{align*}
X_{\fie{p}}^\trans &= M\cdot (p_1,p_2,p_3)^\trans\\
&= \begin{pmatrix}
0 & -\omega_3 & \omega_2\\
\omega_3 & 0 & -\omega_1\\
-\omega_2 & \omega_1 & 0
\end{pmatrix}\cdot (p_1,p_2,p_3)^\trans\\
&= (-p_2\omega_3 + p_3\omega_2, -p_3\omega_1 + p_1\omega_3, -p_1\omega_2 + p_2\omega_1)^\trans.
\end{align*}
Legyen $\bm{\omega}=(\omega_1,\omega_2,\omega_2)$, ekkor $X_{\fie{p}}=\bm{\omega}\times\fie{p}$.
Továbbá az $\bm{\omega}$ vektort könnyű meghatározni, mivel
\[
B_{\fie{w}}(t)=\fie{w} \;\text{minden}\;t\;\text{esetén} \;\impli\; 0=X_{\fie{w}}=\bm{\omega}\times\fie{w},
\]
így $\bm{\omega}$ a $\fie{w}$ egy skalárszorosa, és könnyű leellenőrizni egy alkalmasan
valásztott vektorral, hogy valójában $\bm{\omega}=\fie{w}$. Mondhatnánk tehát azt is,
hogy az $\times$ vektoriális szorzat azért speciális $\Rs$-ban, mert az $n=3$ az egyetlen
dimenzió, amikor $O(n)$ dimenziója éppen $n$. A lényegre térve:

\begin{propnn}
$\Rs$-ban a tengely körüli forgatások által generált vektormezők 
$\fie{p}\mapsto \bm{\omega}\times\fie{p}$ alakúak, ahol $\bm{\omega}\in\Rs$.
\end{propnn}

Egy $\fie{v}$ sebességvektorú $c$ részecske esetén tekinthetjük a $c\times\fie{v}$
függvényt amely $\Real$-ből képez $\Rs$-ba, és nevezzük ezt a részecske
\textbf{szögsebesség-vektorának}; ha $c(t)=(x(t),y(t),z(t))$ adottt $x$, $y$, és $z$ függvényekre,
akkor a $c$ szögsebessége
\[
\tag{$\mathrm{A}_c$} (yz'-y'z,\, x'z-xz',\, xy'-x'y).
\]
Egy $m$ tömegű részecske esetén az $\fie{L}=c\times m\fie{v}$ vektoriális szorzatot
a részecske \textbf{impulzusmomentumának} nevezzük. A szögsebesség-vektort és az
impulzusmomentumot a ,,a 0 origóhoz képest'' definiáltuk: tetszőleges $P$
ponthoz képest
\[
\fie{L}_P=(c-P)\times m\fie{v}.
\]

Egy $(c_1,\ldots,c_K)$ részecskerendszer esetén az rendszer 0-hoz viszonyított
$\fie{L}$ impulzusmomentumát az
\[
\fie{L}=\sum_{i=1}^K c_i\times m_i\fie{v}_i
\]
kifejezéssel definiáljuk, és most természetesen muszáj az összes $c_i\times m_i\fie{v}_i$
vektort egy adott pontban érteni, azaz nem különböző pontokban felvett érintővektoroknak
tekintjük őket. Vegyük észre, hogy az $\fie{L}'=\sum_{i=1}^K (c_i\times m_ic_i')'$ egyenlet
egyszerűen
\[
\tag{$\mathrm{L}'$} \fie{L}'=\sum_{i=1}^K c_i\times m_ic_i''.
\]

Az általános esetben, a $P$ pontra vonatkoztatott $\fie{L}_P$ impulzusmomentumot az
$\fie{L}_P=\sum_{i=1}^K (c_i-P)\times m_i\fie{v}_i$ kifejezéssel definiáljuk. 
Speciálisan, legyen a $P$ pont a rendszer $C$ tömegközéppontja (ez azt jelenti, hogy
az impulzusmomentumot a különböző időpontokban különböző pontra vonatkoztatva
tekintjük). Legyen $M=\sum_i m_i$ a $C$ részecske ,,tömege'', ekkor
\begin{align*}
\sum_i m_ic_i\times\fie{v}_i&=\sum_i m_i(c_i-C)\times\fie{v}_i + \sum_i m_iC\times \fie{v}_i\\
&=\fie{L}_C + \Big[C\times \big(\textstyle{\sum_i m_i\fie{v}_i}\big) \Big]\\
&=\fie{L}_C + [C\times MC\,'\,],
\end{align*}
ezért
\[
\fie{L}=\fie{L}_C + (C\times MC\,').
\]
Az $\fie{L}_C$ tömegközéppontra vonatkoztatott impulzusmumentumot
,,sajátperdületnek'', a tömegközéppont origóra vonatkoztatott $C\times MC\,'$ 
impulzusmomentumát pedig ,,pálya-impulzusmomentum'' szokás nevezni. 
Az iménti egyenlet tehát azt fejezi ki, hogy az $\fie{L}$ össz-impulzusmomentum
egyenlő a sajátperdület és a pálya-impulzusmomentum összegével.

Ha impulzus helyett a $c$ pontban egy tetszőleges $\fie{F}$ erőt tekintünk, akkor a
\[
\bm{\tau}=c\times \fie{F}
\]
vektoriális szorzatot a 0-ra vonatkoztatott \textbf{forgatónyomatéknak} nevezzük,
illetve $\bm{\tau}_P=(c-P)\times\fie{F}$ a $P$-re vonatkoztatott forgatónyomaték.
(Bár az impulzusmomentumra fizikusok által használt $\fie{L}$ jelölést használom,
képtelen vagyok rávenni magam, hogy a forgatónyomatékot $\fie{N}$-nel jelöljem.)

Hasonlóan definiálható részecskerendszer forgatónyomatéka; most is kénytelenek
vagyunk az egyes részecskékre ható forgatónyomatékokat egy pontból mutató vektoroknak képzelni,
bár az egyes erők különböző pontokban hatnak.

\begin{prop}[IMPULZUSMOMENTUM-TÉTEL]
Ha rendszerünkre érvényes a harmadik törvény erős alakja, akkor az össz-forgatónyomaték
megegyezik az össze-impulzusmomentum deriváltjával,
\[
\bm{\tau}=\fie{L}'.
\]
\end{prop}
\begin{proof}
Egyrészt
\begin{align*}
\fie{L}'&=\sum_i c_i\times m_ic_i''\quad\quad\text{az $(\mathrm{L}')$ egyenlet miatt}\\
&=\sum_i c_i\times\fie{F}_i\\
&=\sum_i c_i\times\fie{F}_i^e + \sum_i\sum_j c_i\times\fie{F}_{ij}\\
&=\bm{\tau} + \sum_i\sum_j c_i\times\fie{F}_{ij}.
\end{align*}
Másrészt a harmadik törvény erős alakja miatt
\[
\fie{F}_{ij}=\lambda_{ij}(c_i-c_j),
\]
ahol $\lambda_{ij}=\lambda_{ji}$, ezért
\[
\sum_i\sum_j c_i\times\fie{F}_{ij}=\sum_i\sum_j \lambda_{ij}[c_i\times c_i -c_i\times c_j],
\]
ami zérus, mivel $c_i\times c_i=0$ és $c_i\times c_j=-c_j\times c_i$ és $\lambda_{ij}=\lambda_{ji}$.
\end{proof}

Egyerűen adódik az álalánosabb

\begin{coro}
Bármely $P$ pontra
\[
\bm{\tau}_P=\fie{L}_P'.
\]
\end{coro}

Speciálisan, ha a forgatónyomaték 0, akkor az impulzusmomentum megmarad.
Ez az helyzet áll elő, amikor egyetlen részecske \emph{centrális erő} hatására mozog,
ekkor $\fie{F}$ a $c$ egy skalárszorosa, ezért $\bm{\tau}=c\times\fie{F}=0$.
Ezt már Newton is megfigyelte \apageref{teruletall}. oldalon szereplő 1. Állítás
következményeként:
\begin{quote}
1. K{\footnotesize ÖVETKEZMÉNY.} Ellenállásmentes térben egy rögzített középpont által
vonzott test sebesesség fordítottan arányos a középpontból a pálya érintőegyenesére
bocsátott merőleges hosszával.
\end{quote}
A 2. fejezetben beláttuk, hogy a részecske valójában egy síkban marad, ezért 
mondjuk $c(t)=(x(t),y(t),0)$ esetén az impulzusmomentum-megmaradás azt állítja,
az $(\mathrm{A}_c)$ egyenlet miatt, hogy $xy'-yx'$ állandó. Jóval később mondták ki
a némileg általánosabb törvényt, amely szerint külső erők hiányában az 
impulzusmomentum megmarad, és ez sokáig csak ,,területtételként'' ismerték,
 németül \emph{Fl\"achensatz}.

A forgatónyomaték latinul \emph{torquere} (csavarni), ez utóbbi kifejezést csak a 19. század
végén vezették be. Korábban a $c\times\fie{F}$ a vektoriális szorzatot az $\fie{F}$ erő
$c$ pontbeli, 0-ra vonatkoztatott \textbf{momentumának} nevezték. 
Itt a ,,momentum'' kifejezést a ,,fontos'' vagy ,,lényeges'' értelemben kell érteni,
és ezt a jelentőséget már jóval korábban, az emelőtörvénynél már felismerték. 

Az impulzusmomentum-megmaradás szokásos, elemi demonstrációját egy forgószéken ülő
személlyel szokták elvégezni, aki a kitárt karjaiban súlyokat tart, 
eléggé nagymértékben meg tudja növelni a forgásának a sebességét csupán azzal, hogy
\begin{center}
\includegraphics[scale=0.3]{fig/forgoszek.png}
\end{center}
magához húzza a súlyokat. Teljesen hasonló az, ahogyan jégkorcsolyások felgyorsítják
a forgásuk sebességét azáltal, hogy magukhoz húzzák a karjukat; a műugrók kis 
impulzusmomentummal kezdik az ugrásukat, aztán hirtelen felgyorsítják magukat azzal,
hogy magukhoz húzzák a karjukat és a térdüket; továbbá a tornászok is mindenféle
trükköt bevetnek.

Megjegyezzük, hogy az impulzusmomentum-memgmaradásra való hivatkozás nélkül is meg tudjuk
magyarázni a felgyorsulást, csupán az erők összeadására vonatkozó paralelogrammaszabály
fehasználásával: a súly már meglévő $\fie{v}$ sebességének és a befeléhúzás során nyert 
$\fie{w}$ sebességnek az összege
\begin{center}
\includegraphics[scale=0.3]{fig/impulzusmomentumpara.png}
\end{center}
az e két vektor által kifeszített téglalap átlója, következésképpen hosszabb.

Ezekben a példákban csupán egy meglévő, nemzérus impulzusmomentumot változtattunk meg,
de valami nagyon érdekes dolog történik, amikor 0 impulzusmomentumról indulunk.
A súlyokat egy irányban, egy körön mozgatva ad valamilyen impulzusmomentumot
a súly-plusz-személy rendszernek, amelyet ki kell kompenzálnia egy ellentétes
irányú impulzusmomentummal a rendszernek, tehát az ülő személynek az ellenkező irányban 
kell elfordulnia. A mozgás végén a személy már egy másik irányba fog nézni.
A macskák ugyanezt a jelenséget használják ki ahhoz, hogy átforduljanak, amikor 
fejjel lefelé ledobjuk őket -- ezért mindig a lábukra érkeznek.

Ebben a tekintetben a forgómozgás nagyon más, mint az egyenes mentén való elmozdulás. 
Kizárólag belső erőkkel nem tudjuk egy részecskerendszer helyzetét megváltoztatni,
ehhez mindenképpen szükség van külső erőkre. Egy teljesen súrlódásmentes jégfelületen 
el tudunk fordulni, viszont a tömegközéppontunkat nem tudjuk elmozdítani. 
(Persze erős kilégzéssel, mintegy lökhajtással el tudunk mozdulni -- felhasználva,
hogy a tüdőnkben lévő levegő egy olyan rendszer része, amihez mi nem vagyunk rögzítve.
Vagy egyszerűen eldobhatjuk a kabátunkat.)

Az impulzus-, és az impulzusmomentum-tétel a mechanika első két megmaradási tétele, 
amelyek minden mechanikai rendszerre érvényesek, bár alkalmazásukhoz a rendszerre vonatkozó
további ismeretekre lehet szükség. Megemlítjük, hogy mindkét megmaradási törvény
vektoregyenlet, ezért ha mondjuk az $\fie{F}$ teljes külső erő $x$-komponense zérus,
akkor az összimpulzus $x$-komponense konstans. Általánosabban fogalmazva, 
ha az $\fie{F}$ teljes külső erő egy bizonyos irányba eső komponense zérus, akkor
az összimpulzus ugyanabba az irányba eső komponense konstans. 
Ez most nem tűnik tűnik túl hasznosnak, de az impulzusmomentumra vonatkozó analóg állítás
már az (5. feladat): ha a forgatónyomaték egy adott irányba eső komponense zérus,
akkor az impulzusmomentum ugyanabba az irányba eső komponense konstans.

A harmadik megmaradási törvény eléggé eltérő jellegű az első kettőtől;
egyszerre speciálisabb és általánosabb.

\section{Az energiamegmaradás: a kinetikus-, és a potenciális energia}

Galilei észrevétele alapján egy nehézségi gyorsulás hatására zuhanó,
kezdetben nyugalomban lévő test által megtett $s$ útra $s=at^2$ érvényes,
alkalmas $a$ esetén, tehát $v=s'=2at$ és ezzel $v$ az idő helyett kifejezhető a
megtett út segítségével:  $v^2=\text{konstans}\cdot s$.

Az általánosabb vizsgálhathoz tételezzük fel, hogy a test egy olyen erő hatására
zuhan, amely csak a test földfelszíntől mért $x$ magasságától függ:
\[
mx''(t)=-f(x(t)),
\]
ahol $f$ tetszőleges függvény. (A 2/6. feladathoz hasonlóan a $-$ előjelet azért
írtuk oda, hogy pozitív $f$ a Föld felé húzó vonzóerőnek feleljen meg.)
Bár nem biztos, hogy expliciten ki tudjuk fejezni $x$-et, mégis ki tudunk nyerni 
némi információt $v=x'$-re vonatkozóan. Ehhez a következő nyilvánvaló trükköt alkalmazzuk:
szorozzuk meg az iménti egyenlet mindkét oldalát $x'(t)$-vel, így annak jobb oldala
egy derivált lesz:
\[\label{trukkenergia}
mx'(t)x''(t)=-f(x(t))\cdot x'(t)=-(F\circ x)'(t)\quad\text{ahol}\;\; F\,'=f.
\]
Vegyük észre, hogy a bal oldal is egy derivált. Ekkor azt kapjuk, hogy
\[
\big(\tfrac{1}{2}mx'^2\big)'=-(F\circ x)'.
\]
A $T=\tfrac{1}{2}mv^2$ mennyiséget a test \textbf{kinetikus}-, vagy \textbf{mozgási energiájának}
nevezzük. Tehát, ha $x_i=x(t_i)$, ahol $i=0,1$ és $v_i=v(t_i)=x'(t_i)$, akkor
\[
\tag{*} T(t_1)-T(t_0)=\tfrac{1}{2} mv_1^2 - \frac{1}{2} mv_0^2= -F(x_1) + F(x_0),
\]
azaz a kinetikus energia különböző időpillanatokban vett $T(t_1)-T(t_0)$ eltérése
csak a két időpillatban vett magasságoktól függ.

Zárójelben megjegyezzük, hogy egy másik út is járható, amennyiben már előre tudjuk,
hogy $x$ függvényeként keressük $v$-t. \Apageref{leibnizrugalmas}. oldalon látottakhoz hasonlóan
az $mv'(t)=-f(x(t))$ egyenletből kifejezhetjük $dv/dx$-et:
\begin{align*}
\frac{dv}{dx}&=\left.\frac{dv}{dt}\right/\frac{dx}{dt}=\frac{-f(x)}{mv}\\
-f(x)&=mv\frac{dv}{dx}=\frac{1}{2} m\frac{d(v^2)}{dx}\\
\tfrac{1}{2}mv^2&=\int f(x)\,dx.
\end{align*}

Eddig az egydimenziós szituációt vizsgáltuk, vagy ezzel ekvivalens módon azt az esetet,
amikor az erőnk mindig egy irányba mutat, ám ugyanezek a következetések érvényesek
a még általánosabb, radiálisan szimmetrikus, centrális erők esetén is. 
Ehhez vezessük be az $(r,\theta)$ polárkoordinátákat azon a síkon, ahol a mozgás történik.
Legyen $\fie{r}$ az egységnyi hosszúságú, az origó felé mutató vektormező, valamint
$\bm{\theta}$ a merőleges, szintén egységnyi hosszúságú vektormező. Tetszőleges $x$
pontban a radiálisan szimmetrikus centrális erőnk $-f(|x|)\fie{r}$, ahol $f$ adott függvény.
Ha szokásos módon $r\circ c$ és $\theta\circ c$ helyett $r$-t és $\theta$-t írunk,
akkor
\begin{align*}
\tfrac{1}{2} m(v^2)'&=\tfrac{1}{2}m\cdot \dua{\fie{v}}{\fie{v}}'= m\fie{\fie{v}}{\fie{v}'}
=\dua{\fie{v}}{m\fie{v}'}\\
&=\dua{\fie{v}}{-(f\circ r)\fie{r}}\\
&=\dua{r'\fie{r} +\theta'\bm{\theta}}{-(f\circ r)\fie{r}}\\
&=-(f\circ r)r'=-(F\circ r)', \quad \text{ahol} \;\; F\,'=f.
\end{align*}
Ennélfogva
\[\label{kineradi}
\tag{$**$} T(t_1)-T(t_0)=-F(r(t_1)) + F(r(t_0)),
\]
tehát a kinetikus energia két különböző időpillanatban vett megváltozása csak az
origótól mért, a két időpillatbanban vett távolságtól függ.

Manapság az iménti eredményt másképpen szokás megfogalmazni. Legyen $\Map{V}{\Rs}{\Real}$
a
\[
V=F\circ r
\]
függvény, ekkor $(**)$ átírható a 
\begin{align*}
T(t_1)-T(t_0)&=-F(r(c(t_1))) + F(r(c(t_0)))\\
&=-V(c(t_1)) + V(c(t_0))
\end{align*}
alakba. A $t_0$ lerögzítésével azt kapjuk, hogy
\[
\tag{$***$} T(t) + V(c(t)) \;\;\text{konstans}.
\]
A $V(p)$ mennyiséget a $p\in\Rs$ részecske \textbf{potenciális}-, vagy \textbf{helyzeti
energiájának nevezzük}. Ezzel az \textbf{energiamegmaradást} kifejező $(***)$ egyenlet
azt állítja, hogy radiálisan szimmetrikus, centrális erők esetén a kinetikus-, és a
potenciális energia összege konstans egy részecske pályája mentén. 
Itt lényeges, hogy $V(c(t))$ csak a részecske $c(t)$ helyétől függ, magától a $c$
pályától nem.

Nyilván a $V$ függvény csak egy additív konstans erejéig van meghatározva. 
Egyszerű, a föld közelében szabadon eső testekkel kapcsolatos feladatokban szokás
$V$-t úgy megválasztani, hogy értéke 0 legyen a földfelszín közelében, ekkor tehát
$V$ pozitív, ha testet pozitív magasággból engedjük el. Miközben a test zuhan,
a potenciális energiája lecsökken, miközben a kinetikus energiája növekszik. 
Ez jól magyarázza $V$ szokásos értelmezését, miszerint $V$ a test olyan kinetikus energiája
amivel ,,potenciálisan'' rendelkezik, azaz ezt a kinetkus energiát kinyerheti, ha
szabadon esik a föld felé.

Inverz négyzetes erők esetén egy radiálisan a centrum felé zuhanó test középpontól
vett $r(t)$ távolságára
\[
r\,''(t)=-K/r(t)^2,
\]
és így
\[
v(t)=r'(t)=K/r\;\impli\; V(p)=-\frac{K}{r(p)} + \text{konstans}.
\]
Itt is kényelmes a konstanst $0$-nak választani, tehát $V$ 0 a $\infty$-ben.
Egy ellipszispályán keringő bolygó esetében $V$ nagyobb (bár még mindig negatív) a 
Naptól távolabb, tehát itt a kinetikus energia is kisebb (ez következik Kepler második
törvényéből is).
\begin{center}
\includegraphics[scale=0.3]{fig/kisvnagyv.png}
\end{center}

Általában, azt mondjuk, hogy egy $\fie{F}=(F_1,F_2,F_3)$ erő \textbf{konzervatív},
és $V$ a \textbf{potenciálja}, ha az
\[
\tag{$\mathrm{C}$} \tfrac{1}{2}m\dua{\fie{v}(t)}{\fie{v}(t)} + V(c(t))=\text{konstans}
\]
energiamegaradást kifejező egyenlet minden $\fie{F}$ hatására mozgó $c(t)$ részecskére
érvényes. Az $\Rs$ $(x^1,x^2,x^3)$ standard koordinátarendszerében az iménti relációt 
deriválva,
\begin{align*}
0&=\dua{\fie{v}(t)}{m\fie{v}'(t)} + \frac{d}{dt}V(c(t))\\
&=\dua{\fie{v}(t)}{m\fie{F}(c(t)))} + \sum_{i=1}^3 \frac{\partial V}{\partial x^i} (c(t))
\cdot c_i'(t).
\end{align*}
Egy olyan $c$ görbét válasszunk, amelyre $c(0)=p$ és az értékeljük ki az iménti
relációt a $t=0$ időpillanatban, ekkor
\[
0=\dua{\fie{v}(0)}{\fie{F}(p)} + \sum_{i=1}^3 \frac{\partial V}{\partial x^i}(p)\cdot c_i'(0).
\]
Minthogy az $\Rs$ érintőterei a szokásos módon azonosíthatóak $\Rs$-mal, ezért
\[
\fie{v}(0)=(c_1'(0),c_2'(0),c_3'(0)).
\]
Azt látjuk, hogy ha olyan $c$ görbéket választunk, 
hogy csak egy $c_i'(0)\neq 0$, akkor szükségképpen
\[
\tag{$\mathrm{C}'$} \fie{F}=(F_1,F_2,F_3)=-\Bigg(\frac{\partial V}{\partial x^1},\,
\frac{\partial V}{\partial x^2},\,\frac{\partial V}{\partial x^3}\Bigg),
\]
amit a fizikusok általában az
\[
\fie{F}=-\text{grad}\, V
\]
alakban írnak. Ezzel ekvivalens módon,
\[
\autodua{\fie{F}}{\frac{\partial}{\partial x^i}}=F_i=-\frac{\partial}{\partial x^i}(V),
\]
és így minden $\fie{v}$ érintővektor esetén
\[
\dua{\fie{F}}{\fie{v}}=-\fie{v}(V),
\]
\hlc[green]{ahol a jobb oldalon a szokásos módon hat a $\fie{v}$ érintővektor a $V$ skalárfüggvényre.}

Megfordítva, tegyük fel, hogy $\fie{F}$-re érvényes $\mathrm{C}'$. Először is,
tetszőleges $\fie{F}=(F_1,F_2,F_3)$ mező esetén
\begin{align*}
T(t_1)-T(t_0)&=\tfrac{1}{2}mv_1^2 - \tfrac{1}{2} mv_0^2\\
&=\int_{t_0}^{t_1} \dua{\fie{v}(t)}{\fie{F}(c(t))}\,dt\\
&=\int_{t_0}^{t_1} \sum_{i=1}^3 c_i'(t) \cdot F_i(c(t)).
\end{align*}
Ha most bevezetjük az
\[
\omega=F_1dx^1 + F_2dx^2 + F_3dx^3
\]
\hlc[green]{1-formát}, és a $\gamma=\restr{c}{[t_0,t_1]}$ görbét, akkor az iménti 
azonosság a
\[\label{kinetomega}
\tag{$*$}  T(t_1)-T(t_0)=\int_\gamma \omega
\]
alakban írható. Tehát, ha $\fie{F}$-re érvényes $\mathrm{C}'$, akkor 
\hlc[green]{$\omega=-dV$}, így
\begin{align*}
T(t_1)-T(t_0)&=\int_\gamma -dV\\
&=-V(c(t_1)) + V(c(t_0)),
\end{align*}
ebből pedig már következik, hogy $\fie{F}$ konzervatív, és $V$ egy potenciálja.

Az
\[
\int_{t_0}^{t_1} \dua{\fie{v}(t)}{\fie{F}(c(t))}\,dt=\int_\gamma \omega
\]
mennyiséget az $\fie{F}$ erő által a $\gamma$ görbén mozgó $c$ részecskén 
végzett \textbf{munkának} nevezzük. Az imént láttuk, hogy konzervatív erők esetén
ez csak a pálya végpontjaitól függ. Egy egyszerű példa gyanánt tekintsük egy 
zárt ellipszispályán, a $[0,T_0]$ időintervallum során mozgó részecskét, amely egy
$\fie{F}$ inverz négyzetes erő hat. Az $\fie{F}$ által végzett összmunka muszáj, hogy 0 legyen
a pálya mentén, elvégre az egyenlő $[0,0]$ időintervallum során elvégzett összmunkával,
ami 0.

Általában persze nem szokott egynél több pálya lenni a két pont között. 
Az érdekes eset az -- és ez jobban kapcsolódik a mindennapi munka-fogalmunkhoz --,
amikor arra a munkára vagyunk kíváncsiak amit az $\fie{F}$ erőtér ellénben kell 
elvégezni, hogy a részecskét az egyik pontból a másikba juttassuk el.
Például egy $m$ tömegű részecske $h_0$ magasságból $h_1$ magasságba való (a földfelszín
közelében maradva) felemeléséhez szükséges munka $mg(h_1-h_0)$, attól függetlenül,
hogy milyen pályán végezzük az emelést. Ezt persze úgy kell érteni, hogy a
reszecske esetleges lefelé való mozgatása során végzett munkát negatív előjellel vesszük.

A fizikakönyvekben a konzervatív erők iménti tulajdonságát tekintik a definíciónak,
azaz azt mondják, hogy $\fie{F}$ konzervatív, ha $\int_\gamma \omega$ csak a
$\gamma$ végpontjaitól függ. Ebből rögtön következik a mi definíciónk. Legyen ugyanis
\[
V(p)=\int_\gamma \omega,
\]
ahol $\gamma$ egy tetszőleges $p_0$-t és $p$-t összekötő görbe, ekkor \apageref{kinetomega}.
oldal $(*)$ egyenletéből rögtön kapjuk az energiamegmaradást:
\[
\tag{$(\mathrm{C})$} \tfrac{1}{2}\dua{\fie{v}(t)}{\fie{v}(t)} + V(c(t))=\text{konstans}.
\]

\Apageref{kineradi}. oldal $(**)$ egyenletéhez vezető számolás akkor is érvényes,
ha $\fie{F}$-et $\fie{F}+\fie{F}_1$-re cseréljük, ahol $\fie{F}_1$
merőleges $\fie{v}$-re. A fizikusok ilyenkor azt mondják, hogy az $\fie{F}_1$
erő által végzett munka 0, elvégre az mindig merőleges $\fie{v}$-re.

Most tekintsük az ingamozgást, amelyet már felületesen elemeztünk az 1/17. feladatban.
\begin{center}
\includegraphics[scale=0.3]{fig/ingaenergia.png}
\end{center}
Az ingafejre ható összerő $\overline{\fie{F}}=\fie{F}-\fie{F}_1$, ahol $\fie{F}$ a
konzervatív nehézségi erő, és $\fie{F}_1$ a korábban bevezetett erő, amely a zsinór
irányában hat. Ezért $\dua{\fie{v}}{\fie{F}_1}=0$, tehát továbbra is érvényes a
$(\mathrm{C})$ energiamegmaradás.

Bár nem tudjuk expliciten megoldani \apageref{ingaegyenlet}. oldalon levezetett
ingaegyenletet, 
\begin{center}
\includegraphics[scale=0.3]{fig/ingaenergia2.png}
\end{center}
mégis meghatározhatjuk, az ingafej $v$ sebességét a $h$ magasságban, elvégre
\[
mgh + \tfrac{1}{2}mv^2=\text{konstans}
\]
érvényes. Tehát csak azt kell tudnunk, hogy milyen $h_0$ magasságból engedtük el $v=0$
kezdősebsség mellett az ingát (vesd össze ezt az 1/17. feladattal!).
Az inga ennélfogva tekinthető egy olyan mechanizmusnak, amely folyamatosan egymásba alakítja
a potenciális-, és a kinetikus energiát. A lengés tetején a kinetikus energia 0,
a lengés alján a potenciális energiakülönbség kinetikus energiává alakul, ami éppen
elegendő ahhoz, hogy ugyanolyan magasra felemelkedjen az ingafej, mint ahonnan indult.

\Apageref{lejtokocka}. oldalon a lejtőn lecsúszó kockát elemi módon tárgyaltuk, 
ahol feltételeztük, hogy a kockára az $\fie{F}$ nehézségi erő és egy másik, 
$-\fie{F}_1$ erő hat, amely merőleges a lejtőre. Tehát a fenti gondolatmenet ebben az esetben
is működik, és azt kapjuk, hogy a lejtő alján a kocka $\tfrac{1}{2}mv^2$ kinetikus energiája 
ismét megegyezik $gh$-val. Így a kocka lejtő alján mért sebessége meg kell, hogy egyezzen
azzal a sebességgel, amivel akkor rendelkezne, ha egyszerűen leejtettük volna $h$ magasságból.
Ez pedig összhangban van a számításainkkal, ugyanis a kocka lejtőmenti gyorsulása a
$\sin\alpha$-szorosa a szabadesés közbeni gyorsulásával, de a lejtőn $1/\sin\alpha$-szor
akkora távot kell megtennie. Teljesen hasonlóan, egy zsinegen való felfüggesztés helyett
az ingafej egy síkban fekvő körpályán is haladhat, sőt, akármilyen súrlódásmentes pályán is.

Az energiamegmaradás a nyílvánvaló fizikai hasznán túlmenően matematikailag is 
fontos, ahol a mozgástörvények ,,első integráljának'' nevezik, azaz olyan
egyenlet, amelyben a második deriváltak nem, csakis az első deriváltak szerepelnek 
-- ez pedig mind \apageref{trukkenergia}. oldalon elsütött trükknek köszönhető.
A következő fejezetben a 2/6. feladat eredményeit sokkal szisztematikusabban, az
energiamegmaradásból kiindulva fogjuk levezetni, és ott ki fog derülni, hogy egy 
körpálya $E$ összenergiájának előjele egyszerű geometriai jelentéssel bír.

\section{Energiamegmaradás ütközések során}

Ugyan konzervatív erők esetén eléggé nyilvánvaló a kinetikus energia szerepe, 
kezdetben igen nagy értetlenség övezte a kinetikus energiát az ütközésekben betöltött,
teljesen más jellegű szerepe miatt.

Tekintsünk két részecskét: $c_1$ és $c_2$, amelyek tömege $m_1$ és $m_2$. 
Tegyük fel, hogy ezek egy egyenes mentén $v_1$ és $v_2$ sebességgel mozognak. 
Adódik a következő természetes kérdes: mi az ütközésük utáni $w_1$ és $w_2$ sebességük?

Az impulzusmegmaradás csupán egy egyenletet ad,
\[
\tag{1} m_1w_1+m_2w_2=m_1v_1+m_2v_2,
\]
azonban két ismeretlenünk van: $w_1$ és $w_2$. Ennélfogva ez nem elég a kérdés megválaszolására,
még az $m_1=m_2$ és $v_2=0$ feltevések mellett sem, amely két azonos tömegű tárgy ütközésére
vonatkozik, amelyek közül az egyik nyugalomban van. Egy lehetséges megoldás $w_1=0$ és $w_2=v_1$,
azaz az első test megáll, és minden mozgását átadja a másodiknak (valami ehhez hasonló
történik, amikor két acélgolyó utközik). Másrészt a második test akár ,,puha'' is lehet, például
agyag, ami a becsapodástól meghajlik és hozzáragad az elsőhöz, így az ütközés után a két
test egybeolvadva mozog (ehhez hasonló \apageref{ragadkocka}. oldalon tárgyalt, összeragadó
testek esete; vagy olyan vasúti kocsik, amelyek összecsatlakoznak ütközés során). 
Ebben az esetben a két test ütközés utáni sebessége egyszerűen $v_1/2$. 
Persze ez is csak egy a végtelen sok lehetséges megoldás közül.

Egy elemi fizikakönyvnek nyilván olyan feladatokat kell kitüznie, amelyeknek  van
megoldása. Ezért -- mint amikor egy buli házigazdája közönyösen bemutat egy hírességet--, 
a fizikakönyvek becsúsztatnak egy új definíciót: az ütközést ,,teljesen rugalmasnak''
nevezzük, ha a kinetikus energia is megmarad,
\[
\tag{2} \left\{\begin{aligned}
m_1w_1^2 + m_2w_2^2 &= m_1v_1^2 + m_2v_2^2 \\
&\text{azaz}\\
m_1(w_1^2-v_1^2)&=m_2(v_2^2-w_2^2)
\end{aligned}\right.
\]
A definíció mintegy ellenpolólusként követi a következő: egy ütközés ,,teljesen rugalmatlan'',
ha $w_1=w_2$ (azaz a két test összeragad).

Amint rendelkezésünkre áll a definíció, mindenféle egyszerű, ,,teljesen rugalmas'' ütközésekre
vonatkozó feladatot lehet kitűzni, akármit is jelentsen ez a fogalom. Általában, ha az $(1)$
egyenletet az
\[
m_1(v_1-w_1)=m_2(w_2-v_2)
\]
alakba írjuk, és ezt elosztjuk a $(2)$ második alakjával, akkor azt kapjuk, hogy
\[
\tag{3} w_1-w_2=-(v_1-v_2),
\]
hacsak nem $w_1=v_1$ (és $w_2=v_2$). Az $(1)$ és a $(3)$ egyenletet megoldva az ismeretlen 
$w_1$-re és $w_2$-re, kapjuk a
\[
\tag{$*$} \begin{aligned}
w_1&=\frac{m_1-m_2}{m_1+m_2} v_1 + \frac{2m_2}{m_1+m_2} v_2\\
w_2&=\frac{2m_1}{m_1+m_2} v_1 - \frac{m_1-m_2}{m_1+m_2} v_2
\end{aligned}
\]
megoldást. A másik megoldást -- $w_1=v_1$ és $w_2=v_2$ -- fizikailag értelmetlen, és
ezért elvetjük, ugyanis olyan mozgást ír le, amely során a részecskék áthaladnak egymáson.

Az iménti, nem kifejezetten tanulságos formula sokkal értelmesebb lesz, ha bevezetjük
a ,,tömegközépponti koordinátákat'' (10. feladat). Minden esetre, ha $m_1=m_2$ és
$v_1=-v_2$, azaz két, egyforma tömegű részecske ellentétes sebeséggel halad egymás felé,
akkor azt kapjuk, hogy $w_1=v_2$ és $w_2=v_1$, azaz a két részecske ugyanolyan sebességgel
pattan vissza. Ha $m_1=m_2$ és $v_2=0$, akkor $w_1=0$ és $w_2=v_1$, azaz az első 
részecske megáll, és a második az első sebességével halad tovább.

Általános ütközések esetén az $e$ \emph{ütközési tényezőt} a
\[
(w_1-w_2)=-e(v_1-v_2)\quad\text{vagy}\quad e=\frac{w_2-w_1}{v_1-v_2}
\]
egyenlettel. Ez kísérletileg úgy tűnik, hogy némileg független a $v_1$ és $v_2$ kezdeti 
sebességektől. Ezt általában csak akkor alkalmazzák, amikor a két tárgy egymás felé mozog, 
és ellentétes irányban pattannak vissza.  Például, ha $v_1>0$ és $v_2<0$, akkor
$w_1<0$ és $w_2>0$, amiből $e\ge 0$ következik.

Pusztán azzal, hogy definiáltuk az ütközési tényezőt, nem nyerünk semmi újat. Ez csupán
egy lehetséges módja annak, hogy kísérleti tapasztalatokhoz illesszük az elméleti
eredményeinket; ez utóbbiakat pedig a teljesen rugalmas ütközések \emph{ad hoc} definíciójából
nyertük. Továbbra is meg szeretnénk érteni, hogy egy teljesen rugalmas üközés
miért számít egy hallgatólagosan elfogadott koncepció idealizációjának? Azaz 
miért felel meg 
