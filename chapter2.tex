\chapter{A centrális erők Newton-féle elemzése}

\lettrine[lines=2]{A}{} Principia első könyvének címe: ,,A testek mozgása''. Az első
szakasz olyan geometriai meggondolásokkal kezdődik, amelyeket manapság 
határértékekkel fogalmaznánk meg, de a második szaksz rögtön
,,Kepler második törvényével'' indít, bár Newton nem említi Keplert név szerint.

Kepler második törvénye, ami valójában az első volt amit felfedezett, azt mondja ki,
hogy egy bolygó rádiuszvektora egyenlő területeket söpör végig egyenlő idők során,
vagy ezzel ekvivalens módon, a $t$ időpillanatig végigsöpört terület arányos $t$-vel.
\begin{center}
\includegraphics[scale=0.3]{fig/kepler.png}
\end{center}
Newton rámutatott, hogy ez annak a következménye, hogy a Nap által a bolygóra
kifejtett gravitációs vonzóerő mindig a Napot és a bolygót összekötő egyenesbe esik,
vagy ezzel ekvivalens módon, a bolgyó gyorsulása mindig a Nap felé mutat --
és most lényegtelen ennek az erőnek a nagysága. Newton szavaival:
\begin{quote}\label{teruletall}
1. Állítás. \emph{A területek, amelyek a pályán mozgó testeket és egy álló középpontot
összekötő sugarak rajzolnak, álló síkokban vannak és arányosak az eltelt időkkel.}
\end{quote}

Úgy esik, hogy ezt elképesztően könnyű analitikusan bebizonyítani, főleg ha
használjuk a vektoriális szorzatot. Az egyszerűség kedvéért tegyük fel, hogy
az erő mindig az $O$ origó irányába mutat, és legyen $c$ egy tetszőleges részecske.
Ekkor a
\begin{align*}
(c\times\fie{v})'&=(c\times\fie{v}')+(c'\times\fie{v})=(c\times\fie{v}')+(\fie{v}\times\fie{v})\\
&=c\times\fie{v}'
\end{align*}
összefüggés mindig érvényes, tehát ha $\fie{v}'$ a $c$ irányába mutat, akkor 
egyszerűen $(c\times\fie{v})'=0$, következésképpen
\[
\tag{*} c\times\fie{v}=\fie{w}\quad\text{ahol $\fie{w}$ egy konstans vektor.}
\]

Ha $\fie{w}=0$, akor $\fie{v}(t)$ mindig az $O$-t és a $c(t)$-t összekötő egyenes irányába
mutat, és így a részecske egyszerűen az $O$ felé mozog egy egyenes mentén. 
Ha $\fie{w}\neq 0$, akkor a 
\[
0=\dua{c(t)\times\fie{v}(t)}{c(t)}=\dua{\fie{w}}{c(t)}
\]
relációból azonnal látjuk, hogy $c(t)$ egy síkban mozog.

Továbbá, a $c(t)\times\fie{v}(t)$ mennyiséget természetes módon 
a rádiuszvektor által végigsöpört területként lehet értelmezni. Kis $h$ esetén ez a terület
$S(t+h)-S(h)$, közelítőleg
\begin{center}
\includegraphics[scale=0.3]{fig/keplerterulet.png}
\end{center}
a besatírozott háromszög területe, és ezért közelítőleg
\[
\frac{1}{2}\big|c(t)\times[c(t+h)-c(t)]\big|.
\]
Következésképpen határértéket véve
\begin{align*}
S'(t)&=\frac{1}{2}\lim_{h\to 0}\Bigg|c(t)\times \frac{c(t+h)-c(t)}{h}\Bigg|\\
&=\frac{1}{2}|c(t)\times\fie{v}(t)|.
\end{align*}
Tehát $(*)$-ból következik, hogy $S'(t)$ konstans, azaz $S(t)$ arányos $t$-vel.

Az iménti közelítésen alapuló levezetés nem szükséges, ugyanis $c$-t expliciten meg
lehet adni a
\[
c(t)=r(t)\cdot (\cos\theta(t),\sin\theta(t))
\]
alakban, kiszámíthatjuk $\fie{v}=c'$-t és beláthatjuk, hogy
\[
|c\times\fie{v}|=r^2\theta',
\]
és hogy $\tfrac{1}{2}r^2\theta'$ egyszerűen az területszámításhoz szükséges 
integrandus polárkoordinátákban. A későbbiek miatt érdemes az iméntieket rögzíteni:
\[\label{rsqtheta}
\tag{*'} r^2\theta'=|\fie{w}|=h, \;\text{mondjuk}.
\]

Úgy tűnik, hogy nehéz ezen az analitikus bizonyításon javítani, ám Newton
egy olyan geometriai bizonyítást ad az 1. Állításra, amelyben a görbét 
sokszögekkel közelíti. Ez nem csak egyszerű, hanem meg is mutatja, hogy \emph{miért}
igaz az állítás.

Egyfajta szentimentális gesztusként álljon itt a Newton által, a bizonyításhoz használt
rajz másolata
\begin{center}
\includegraphics[scale=0.3]{fig/newtonbiz.png}
\end{center}
bár mi igazából csak egy kis részére fogunk koncentrálni:
\begin{center}
\includegraphics[scale=0.3]{fig/newtonbiz2.png}
\end{center}

Newton feltételezi, hogy a részecske egy $ABCD\ldots$ pályát követ, és közben
rövid időintervallumok során ,,impulzív'' erők hatnak rá, és hogy ezek az impulzív erők
a $B$, $C$, ... pontokban mindig az $S$ irányába mutatnak, és ezért a pálya
az $\lapl SAB$, $\lapl SBC$, ... háromszögeket háromszögeket söpri végig. 
Newtonnak ekkor elég megjegyeznie, hogy ha nem hatnának az impulzív erők,
 akkor a részecske egyszerűen elmenne $c$ pontba, amelyre $Bc=AB$. Ebben az esetben
az $\lapl SBc$ háromszöget söpörné végig, amelynek ugyanannyi a területe, mint
az $\lapl SAB$ háromszögnek (ugyanis egyenlőek az alapjaik és a magasságaik).
Az $B$ pontban ható impulzív erő viszont a $C$ pontba küldi a részcskét, ami a $Bc$
és a $BV$ által meghatározott paralelogramma áltóján van, és $BV$ a $SB$
egyenesben van, ugyanis feltettük, hogy az erő az $S$ felé mutat.
Ez azt jelenti, hogy $Cc$ párhuzamos $BV$-vel, és ez pedig azt jelenti, hogy 
$\lapl SBc$ területe megegyezik $\lapl SBC$ területével (mivel mindkét háromszögnek
$SB$ az alapja, és ugyanaz a magasságuk). Röviden szólva az $\lapl SAB$ területe
ugyanannyi, mint az $\lapl SBC$ területe, és így tovább.\footnote{A Newton
gondolatmenetének korrektségére vonatkozó részletket Pourciau-ban [4] találhatunk.}

Érdemes megemlíteni, hogy Newton kimondja az 1. Állítás megfordítását is, amely
bizonyítása nagyjából ugyanaz. Nem gondolnánk, hogy érdekes ez lenne, és
Newton nem is hivatkozik rá később (ám látni fogjuk, hogy központi szerepe van):
\begin{quote}\label{Newton2allitas}
2. Állítás. \emph{Minden olyan test mozgása, ami valamilyen egy síkba eső görbevonal 
mentén mozog,
és egy adott pontba húzott rádiusz szerint, ... az eltelt időkkel arányos területeket ír le,
azért történik, mert egy centripetális erő afelé a pont felé húzza.}
\end{quote}

Newton Kepler második törvényére adott bizonyítását gyakran közlik az elemi
fizikakönyvek, mivel egyszerű és könnyen áttekinthető. Sajnos ezzel általában véget is ér az 
expozíció, mivel Newton további vizsgálódásaihoz sajnos szükséges számtalan, kúpszeletekre
vonatkozó homályos tulajdonság, amelyeket manapság már senki nem ismer.
Ez dupla csalódás. Bár az érthető, hogy egy geometriai bizonyítás a kúpszeletek számtalan
geometriai tulajdonságát fölhasználja, az igazi rejtély az, hogy mi köze van
egy, az inverz négyzetes törvényre vonatkozó feltevés a kúpszeletek geometriai
tualjdonságaihoz. Továbbá, ugyan nem követjük teljes egészében végig Newton gondolatmenetét,
az derül ki, hogy Newton bizonyításának a \emph{stratégiája} elképesztően ügyes.
\footnote{Annyira ügyes, hogy még az utóbbi időkben is viták tárgyát képezte --
lásd a 7. fejezet utolsó szakaszát.}

Ahhoz, hogy világos legyen, hogy hogyan kapcsolja össze Newton az erőket a geometriával, 
elég követnünk néhány későbbi lépését. Az 1. Állítás ábráján bontsuk fel a mozgást
kicsiny $\delta$ időintervallumokra, és tekintsük a $BB'$ szakaszt, ami a
$B$ és az $AC$ átló $B'$ felezőpontja között van. 
\begin{center}
\includegraphics[scale=0.3]{fig/newtonbiz3.png}
\end{center}
Ez a $BV$ fele, ami az $F$ nagyságú centrális erő által okozott elmozdulást jelképezi,
és ez a távolság egyszerűen $\tfrac{1}{2}F\delta^2$. Így, határértékben
\[
\lim_{\delta\to 0} \frac{1}{\delta^2} BB'=\tfrac{1}{4} F.
\]
Ez Newton pontatlan, bár költőinek hangzó nyelvezetén, ami sokkal gyönyörűbb mint a
határértékek, és az epszilon-delták:
\begin{quote}
Ha ... egy test egy tetszőleges pályán kering egy rögzített középpont körül, 
és egy éppen megszülető, tetszőleges ívet minimálisan rövid idő alatt ír le, és ha
az ív sagittáját úgy húzzuk be úgy, hogy felezze a húrt, és menjen át erőközépponton, 
akkor az ív középpontjában a centripetális erő egyenes arányos a sagittával
 és kétszeresen fordítottan az idővel.
\end{quote}
\begin{center}
\includegraphics[scale=0.3]{fig/newtonbiz4.png}
\end{center}
Nem szükséges magyarázni a régimódi sagitta szót [nyíl Latinul], mivel most
Newton állítása explicite megmondja, hogy az $XY$ szakaszra gondol, 
ami az $S$-en és $AC$ húr $X$ felezőpontján átmenő egyenes fekszik. Az $\tfrac{1}{4}$
tört nem szerepel Newton állításából, mivel az eredményt egy arányosságként mondja ki:
az $A$ és az $A'$ pontok között fellépő centripetális erők aránya ugyanannyi, mint az
$A$ és az $A'$ pontokból induló ívekre vonatkozó $\lim_{\delta\to 0} XY/\delta^2$ határértékek
aránya. 

Ezután Newton vesz egy $P$ pontot egy $S$ köré húzott görbevonalon egy $t$ időpillanatban,
és két $Q$ és $Q'$ pontot, amelyek egymáshoz közeli $t-\delta$ és $t+\delta$ időpillatokban
vannak. 
\begin{center}
\includegraphics[scale=0.3]{fig/newtonbiz5.png}
\end{center}
De az 1. Állítás miatt $\delta$ arányos az (íves) $SPQ$ háromszög területével, és
így határértékben $QT\times SP$-vel, ahol $QT$ merőleges $SP$-re. Ezért az erő a
$P$ pontban arányos a 
\[
\lim_{\delta\to 0} \frac{PX}{(SP)^2\times (QT)^2}
\]
mennyiséggel.

Ám Newton igazából egy olyan ábrát rajzol fel, amiben egy érintő van húzva a $P$ ponthoz,
és egy $QR$ egyenest, ami párhuzamos $SP$-vel, és azt állítja, hogy
\begin{center}
\includegraphics[scale=0.3]{fig/newtonbiz6.png}
\end{center}
$P$-ben az erő arányos a
\[\label{qrspqt}
\tag{*} \lim_{\delta\to 0} \frac{QR}{(SP)^2\times (QT)^2}
\]
mennyiséggel. Persze $QR$ igazából nem egyenlő $PX$-szel, de úgy tűnik, hogy egyértelmű
Newton számára, hogy másodrendben egyenlőek, így a határérték továbbra is érvényes.
\footnote{A pontos állításért, és a valamelyest bonyolult bizonyításért lásd
Pourciau-t [5]}

Mostmár Newton készen áll arra, hogy megmutassa, hogy az inverz négyzetes törvény
hatására mozgó tárgy által leírt pálya egy kúpszelet. Számunkra furcsának tűnhet, 
ahogyan \emph{Newton} kezd neki, ugyanis az állítás egy részleges \emph{megfordítását}
bizonyítja be:
\begin{quote}
\emph{Keringjen a test egy ellipszisen; ekkor található az ellipszis egy fókuszpontja felé mutató
centripetális erő, amely a mozgást okozza.}
\end{quote}
Másképpen fogalmazva, Newton megmutatja, hogy ha adott egy ellipszis mentén haladó $c$ pálya, és 
$c''$ mindig az ellipszis egyik fókusza féle mutat, akkor
\[
|c''(t)|=\frac{k}{d(t)^2} \quad\text{valamely $k$ konstansra},
\]
ahol $d(t)$ a $c(t)$ fókusztól vett távolsága.

A bizonyítás kulcsa a $(*)$ reláció. Valóban, $(*)$ miatt azt állítás ekvivalens azzal,
hogy ellipszis esetén
\[
\lim_{\delta\to 0} \frac{QR}{(QT)^2} \quad\text{állandó.}
\]
Az iménti határértéknek semmi köze nincs az erőkhöz, és az értékét teljes egészében
az ellipszis alakja határozza meg. Akár ki is lehetne számolni a L'H\^ospital-szabály
alkalmazásával: ha $F(\theta)$ jelöli $QR$-et és $G(\delta)$ jelöli $(QT)^2$-et,
akkor $\displaystyle{\lim_{\delta\to 0} F'(\delta)=\lim_{\delta\to 0} G'(\delta)=0}$, és
\[
\lim_{\delta\to 0} \frac{QR}{(QT)^2}=\frac{F''(0)}{G''(0)}.
\]
Ha a viszonylag kellemetlen számolást elvégezzük (7. feladat), 
akkor az derül ki, hogy $F''(0)/G''(0)$
értéke független a $P$ ponttól, sőt valójában $=a/2b^2$ a lenti ellipszis esetében.
A $2b^2/a$ mennyiség az ellipszis klasszikus \emph{latus rectum}-ának hossza,
azaz annak a szakasznak a hossza, amelyet az egyik fókuszon kereszülmenő függőleges
egyenes kimetsz az ellipszisből.
\begin{center}
\includegraphics[scale=0.3]{fig/ellipszisrectum.png}
\end{center}
Newton pontosan ezt az eredményt bizonyítja be geometriailag, és a bizonyítás tényleg
hosszú, komplikált és számos, az ellipszisre vonatkozó állítást használ. 
A teljes bizonyítás megtalálható a Newton [325--330. oldal] könyvben.

Ezek után Newton ad egy hasonló bizonyítást egy hiperbolán mozgó testre, végül egy
bizonyítást egy parabolán mozgó testre.

Rögtön ezután, az eddigiek következményeként kimondja azt az eredményt amit akartunk:
\begin{quote}
1. K{\scriptsize ÖVETKEZMÉNY}. Az iménti három állításból következik, hogy
ha bármely $P$ test egy $P$ helyről elindul egy tetszőleges $PR$ egyenes
mentén, akármekkora sebességgel, és ezzel egyidőben egy centripetális
erő hatást gyakorol rá, amely a helynek az erőközépponttól vett távolságával fordítottan arányos,
akkor ez a test valamely kúpszelet mentén fog mozogni, amelynek az egyik fókuszában
az erőközéppont van. Az állítás megfordítása is igaz.
\end{quote}

A Principia első kiadásában csak \emph{ennyi} szerepel -- azaz, hogy az állítás 
a megfordításainak egy következménye -- de a második kiadásban Newton hozzátett néhány 
mondatot, hogy az olvasót segítse (a harmadik kiadásban egy kicsit átfogalmazta):
\begin{quote}
1. K{\scriptsize ÖVETKEZMÉNY}. Az utóbbi három állításból következik, hogy
ha bármely $P$ test egy $P$ helyről elindul egy tetszőleges $PR$ egyenes
mentén, akármekkora sebességgel, és ezzel egyidőben egy centripetális
erő hatást gyakorol rá, amely a helynek az erőközépponttól vett távolságával fordítottan arányos,
akkor ez a test valamely kúpszelet mentén fog mozogni, amelynek az egyik fókuszában
az erőközéppont van. Az állítás megfordítása is igaz: ugyanis ha adott a fókusz,
az érintési pont és az érintő állása adott, akkor az egy kúpmetszetet határoz meg, 
amelynek adott a görbülete abban a pontban. Ám a görbület a centripetális erőből
és a test sebességéből származik; két különböző pálya, amelyek érintik egymást
nem írható le ugyanazzal a centripetális erővel és ugyanazzal a sebességgel.
\end{quote}

Íme a teljes gondolatmenet némileg eltérő megfogalmazásban. Az egyszerűség kedvéért
$\Real^2$-ben dolgozunk, és tegyük fel, hogy az erők az $O$ origóba mutatnak. 
Adott egy $P$ pont, egy $\fie{v}$ érintővektor a $P$-ben, és keresünk egy $c=(c_1,c_2)$
görbét amelyre $c(0)=P$ és $c'(0)=\fie{v}$, illetve
\[
\tag{$*$} c''(t)=\frac{k}{|c(t)|^2}\cdot \frac{-c(t)}{|c(t)|}
\]
teljesül, ahol $k$ egy adott állandó ($-c(t)/|c(t)|$ tényező egyszerűen egy egység hosszúságú
vektor, amely $c(t)$-ből az origóba mutat).

Minthogy $c'(0)$ adott, és $(*)$ megadja $c''(0)$-t, ezért tudjuk, hogy mennyinek
kell lennie a $c$ görbe $\kappa$ görbületének a $0$-ban, mivel
\begin{equation}\label{kappagorb}
\tag{$**$} \kappa=\frac{c_1'(0)c_2''(0)-c_2'(0)c_1''(0)}{(c_1'^2(0)+c_2'^2(0))^{3/2}}
\end{equation}
Ez a képletet Newton lényegében ismerete, bár a görbületet ő a simulókör segítségével
definiálta.

Most tekintsünk egy $K$ kúpszeletet, amely egyik fókusza $O$, és átmegy $P$-n,
érinti $\fie{v}$-t $P$-ben, illetve a görbülete $P$-ben ez iménti $\kappa$,
és tegyük fel egy pillanatra, hogy ilyen kúpszelet létezik. Tekintsünk egy olyan $\gamma$
görbét, amelyre $\gamma(0)=P$ és úgy járja végig $K$-t, hogy a sugár által kimetszett
területek arányosak az idővel. 
\begin{center}
\includegraphics[scale=0.3]{fig/gorbulet.png}
\end{center}
Az ilyen görbék egy multiplikatív konstanssal való átparaméterezés erejéig meghatározottak;
megfelelő konstans megválasztásával elintézhetjük, hogy $\gamma'(0)=\fie{v}$ 
teljesüljön. A 2. Állítás értelmében, amelyet Newton gondosan leírt, 
\[
\gamma''(t)=\frac{\overline{k}}{|\gamma(t)|^2}\cdot\frac{-\gamma(t)}{|\gamma(t)|},
\]
valamilyen $\overline{k}$ konstanssal. De muszáj, hogy $k=\overline{k}$ teljesüljön,
elvégre $\gamma$-t úgy választottuk meg, hogy a $\kappa$ görbületét a $\gamma(0)=P$
pontban a $(**)$ képlet adja meg.

Tehát $\gamma$ a $(*)$ differenciálegyenlet megoldása, és a megoldás egyértelműsége miatt
(amelyet persze Newton is és minden kortársa implicite feltételezett) ez az
egyetlen megoldás.

\vspace{1em}

Newton gondolatmenete kicsit furcsának hathat amiatt, hogy a kívánt állítás megfordításaiból
indul ki, de egy \emph{geometriai} bizonyításnak szinte mindig ilyen jellegűnek kell lennie:
sokkal egyszerűbb egy geometriai objektumból kiindulni -- egy ellipszisből, egy hiperbolából,
vagy egy parabolából -- és ebből levezetni egy képletet az erőkre, mint a képletből
kiindulni és valahogyan elővarázsolni ezeket a geometriai alakzatokat. 

Az iménti gondolatmenet egyetlen hiányossága az előírt görbületű kúpszelet létezése,
ám Newton kicsit később, az 1. könyv 17. Állításában (ez egy kissé szerencsétlen átrendezés a
korrekt tárgyalási sorrendhez képest) egy részletes geometriai megoldást ad erre a problémára.
Ezzel analóg a $(*)$ egyenlet megoldásainak analitikus leírása. Ezt későbbre, a 4. fejezetre
halasztjuk, ahogyan a bolygómozgás további vonatkozásainak tárgyalását is, mivel ott a releváns
éredményeket összefüggő módon tudjuk tárgyalni.

Azért, hogy legyen valami érzésünk arra vonatkozólag, hogy a pályákat hogyan határozzák meg
a kezdeti feltételek, itt mégis kvalitatíve megvizsgálunk egy speciális esetet, amelyben
a kezdeti $\fie{v}$ sebességvektor merőleges az abba a pontba húzott sugárra. Másképpen
fogalmazva, azt az esetet fogjuk tárgyalni, amikor a kúpszelet egy csúcspontjából indulunk
(pontosabban, az ellipszis esetében a nagytengely végén lévő csúcspontból indulunk).

Tegyük fel, hogy az erő $O$ origó irányába mutat, a nagysága
\[
\frac{1}{|c(t)|^2} \quad \text{(ahol a $k$ konstanst az egyszerűség kedvéért 1-nek vettük)}
\]
és rögzített $c(0)=(R,0)$ ponton átmenő megoldást keresünk, ahol a kezdősebesség $(0,v)$.
\begin{center}
\includegraphics[scale=0.3]{fig/merolegessebesseg.png}
\end{center}
Ebben az esetben könnyű meghatározni $(**)$ (lásd \pageref{kappagorb}. oldal) a $\kappa$ görbületet 0-ban:
\[
\kappa=\frac{1}{v^2R^2}.
\]
Tehát alkalmas kúpszeleteket kell találnunk adott $\kappa$ értékekre, ahol 
$\kappa$ kis értékei nagy $v$ értékeknek felelnek meg, és \emph{vice versa}.

Felhasználva az $a>b$ tengelyű ellipszis
\[
c(t)=(a\cos t,b\sin t)
\]
paraméterezését, könnyű látni, hogy a $\kappa$ görbület a nagytengely végpontjában
\[
\kappa=\frac{a}{b^2}.
\]
Attól függően, hogy az ellipszis középpontjához képest az $(R,0)$ csúcspont és az $O$
fókuszpont hogyan helyezkednek el, két lehetőségünk van: vagy a középpont
ellentétes oldalán vannak, vagy egy oldalon.
\begin{center}
\includegraphics[scale=0.3]{fig/ellipsziskozep.png}
\end{center}
Mindkét esetben
\[
(a-R)^2=c^2=a^2-b^2,
\]
amiből
\[
b^2=2aR-R^2,
\]
tehát a nagytengely végpontjában a görbület
\[
\kappa=\frac{a}{2ar-R^2}\quad a\ge R/2.
\]
Ennek a függvénynek a grafikonja a következő.
\begin{center}
\includegraphics[scale=0.3]{fig/gorbuletgraf.png}
\end{center}
Az $a=R$ esetben $\kappa=1/R$, ami egy kör és erre
\[
\frac{1}{R}=\frac{1}{v^2 R^2},
\]
vagyis $v=\sqrt{1/R}$. Ahogyan csökkentjük $v$-t, az első típusba tartozó
ellipszisek egy családját kapjuk, amelyek az origót és az $(R,0)$ pontot összekötő 
szakasszá degenerálódnak.
\begin{center}
\includegraphics[scale=0.3]{fig/ellipszisszakasz.png}
\end{center}
Ha pedig $v$-t növeljük, a második típusba tartozó ellipszisek egy családját kapjuk,
amelyeknek a csúcspontjában vett görbületei $1/2R$-hez tartanak, és 
az ellipszisek maguk egy parabolához konvergálnak, amelynek a csúcspontjában
vett görbülete szintén $1/2R$.
\begin{center}
\includegraphics[scale=0.3]{fig/ellipszisparabola.png}
\end{center}
Ha tovább növeljük $v$-t, akkor hiperbolák egy családját kapjuk, amelyek az
$(R,0)$ ponton keresztülmenő függőleges egyeneshez tartanak,
\begin{center}
\includegraphics[scale=0.3]{fig/ellipszishiperbola.png}
\end{center}
ami a ,,végtelen nagy'' $v$-nek megfelelő pálya; ez annak az esetnek felel meg,
amikor a gravitációs erő elhanyagolható.